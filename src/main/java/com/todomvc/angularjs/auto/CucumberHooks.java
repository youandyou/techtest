package com.todomvc.angularjs.auto;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.todomvc.angularjs.auto.DriverWrapper.*;
import static com.todomvc.angularjs.auto.steps.StepDefs.logger;
import static com.todomvc.angularjs.auto.utilities.StringUtils.prettifyFeature;
import static com.todomvc.angularjs.auto.vocabulary.Symbols.SEMICOLON;
import static com.todomvc.angularjs.auto.vocabulary.Symbols.UNDERSCORE;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.repeat;

public class CucumberHooks {

	public static boolean previousScenarioFailed = false;

	public static Set<String> beforeFeatures = new LinkedHashSet<>();

	public static StopWatch sw = new StopWatch();

	@Before
	public static void beforeScenario(Scenario scenario) {
		String scenarioId = scenario.getId();
		String feature = scenarioId.split(SEMICOLON, 2)[0];
		String prettyFeature = prettifyFeature(feature);
		int scenarioNumber = 1;
		String[] scenarioNameParts;
		if ((scenarioNameParts = scenarioId.split(repeat(SEMICOLON, 2))).length > 1) {
			scenarioNumber = Integer.valueOf(scenarioNameParts[1]) - 1;
		}
		logger.info("Running [{}] -> [{}]#{}", prettyFeature, scenario.getName(), scenarioNumber);
		String pageLoadStrategyTag = scenario.getSourceTagNames().stream().filter(name -> name.contains("pageLoadStrategy_")).findFirst().orElse(null);
		String pageLoadStrategy = pageLoadStrategyTag != null ? pageLoadStrategyTag.split(UNDERSCORE)[1] : "";
		sw.start();
		initDriver(pageLoadStrategy);
	}

	@After
	public static void afterScenario(Scenario scenario) throws IOException {
		if (scenario.isFailed()) {
			scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
		}
		if (sw.isStarted()) {
			sw.stop();
		}
		logFeatureTime();
		sw.reset();
		quitDriver();
		logger.info(repeat("=", 300));
	}

	public static void logFeatureTime() {
		if (beforeFeatures.size() > 0) {
			System.out.println("");
			logger.info("Feature [{}] took {} seconds to {}",
					prettifyFeature(beforeFeatures.stream().skip(beforeFeatures.size() - 1).findFirst().orElse(null)),
					SECONDS.convert(sw.getNanoTime(), NANOSECONDS),
					previousScenarioFailed ? "fail" : "complete"
			);
		}
	}
}
