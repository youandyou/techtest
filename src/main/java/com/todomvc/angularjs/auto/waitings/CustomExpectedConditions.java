package com.todomvc.angularjs.auto.waitings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.todomvc.angularjs.auto.vocabulary.Symbols.COMMA;
import static java.lang.String.format;

public class CustomExpectedConditions {

	public static ExpectedCondition<Boolean> elementsToBecomeVisible(By... locators) {
		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				boolean result = true;
				for (By locator : locators) {
					result &= driver.findElements(locator).size() > 0 && driver.findElement(locator).isDisplayed();
				}
				return result;
			}

			@Override
			public String toString() {
				return format("All of the following elements to become visible: %s",
						Arrays.stream(locators).map(By::toString).collect(Collectors.joining(COMMA))
				);
			}
		};
	}

	public static ExpectedCondition<Boolean> elementsToBecomeHiddenOrAbsent(By... locators) {
		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				boolean result = true;
				for (By locator : locators) {
					result &= driver.findElements(locator).size() == 0 || !driver.findElement(locator).isDisplayed();
				}
				return result;
			}

			@Override
			public String toString() {
				return format("All of the following elements to become hidden or absent: %s",
						Arrays.stream(locators).map(By::toString).collect(Collectors.joining(COMMA))
				);
			}
		};
	}
}
