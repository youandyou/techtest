package com.todomvc.angularjs.auto.waitings;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class CustomWait extends FluentWait<WebDriver> {

	private TimeUnit timeoutTimeUnit = SECONDS;
	private TimeUnit pollingTimeUnit = MILLISECONDS;

	private int timeout = 30;
	private int pollingInterval = 1000;

	private boolean result;

	private WebDriver driver;

	public CustomWait(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	public CustomWait(WebDriver driver, int timeout) {
		this(driver);
		this.timeout = timeout;
		super.withTimeout(this.timeout, timeoutTimeUnit);
	}

	public CustomWait(WebDriver driver, int timeout, int pollingInterval) {
		this(driver, timeout);
		this.pollingInterval = pollingInterval;
		super.pollingEvery(this.pollingInterval, pollingTimeUnit);
	}

	public boolean successful(Function<? super WebDriver, ?> isTrue) {
		try {
			until(isTrue);
			return result = true;
		} catch (TimeoutException e) {
			return result = false;
		}
	}

	public boolean notSuccessful(Function<? super WebDriver, ?> isTrue) {
		return !successful(isTrue::apply);
	}

	public void setTimeoutTimeUnit(TimeUnit unit) {
		timeoutTimeUnit = unit;
	}

	public void setPollingTimeUnit(TimeUnit unit) {
		pollingTimeUnit = unit;
	}
}
