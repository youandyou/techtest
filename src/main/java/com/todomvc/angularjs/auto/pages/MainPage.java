package com.todomvc.angularjs.auto.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static com.todomvc.angularjs.auto.DriverWrapper.driver;
import static com.todomvc.angularjs.auto.steps.StepDefs.TIMEOUT_PAGE_LOAD;
import static com.todomvc.angularjs.auto.waitings.CustomExpectedConditions.elementsToBecomeHiddenOrAbsent;
import static com.todomvc.angularjs.auto.waitings.CustomExpectedConditions.elementsToBecomeVisible;
import static java.lang.String.format;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.*;

public class MainPage extends PageBase {

	private static final By HEADER_TEXT_NODE = cssSelector("#header h1");
	private static final By NEW_TODO_INPUT = id("new-todo");
	private static final String FILTERS_LOCATOR_STRING = "//ul[@id='filters']";
	private static final By FILTERS = xpath(FILTERS_LOCATOR_STRING);
	private static final String FILTER_BY_NAME_LOCATOR_PATTERN = FILTERS_LOCATOR_STRING.concat("/li[child::a[text()='%s']]");
	private static final String TODO_RECORD_BY_NAME_LOCATOR_PATTERN = "//li[descendant::label[text()='%s']]";
	private static final String TODO_MARK_AS_COMPLETED_CHECKBOX_BY_NAME_LOCATOR_PATTERN =
			TODO_RECORD_BY_NAME_LOCATOR_PATTERN.concat("//input[@type='checkbox']");
	private static final String TODO_LABEL_BY_NAME_LOCATOR_PATTERN =
			TODO_RECORD_BY_NAME_LOCATOR_PATTERN.concat("//label");
	private static final String TODO_REMOVE_BUTTON_BY_NAME_LOCATOR_PATTERN =
			TODO_RECORD_BY_NAME_LOCATOR_PATTERN.concat("//button[@class='destroy']");
	private static final By TODO_COUNT_TEXT_NODE = cssSelector("#todo-count strong");
	private static final By CLEAR_COMPLETED_BUTTON = id("clear-completed");

	public static void assertMainPageIsLoaded() {
		assertTrue(
				format("Main Page wasn't loaded in %d seconds", TIMEOUT_PAGE_LOAD),
				waitFor(TIMEOUT_PAGE_LOAD).successful(elementsToBecomeVisible(HEADER_TEXT_NODE, NEW_TODO_INPUT))
		);
	}

	public static void addTodo(String todoText) {
		WebElement newTodoInput = driver.findElement(NEW_TODO_INPUT);
		newTodoInput.sendKeys(todoText);
		newTodoInput.submit();
	}

	public static void removeTodo(String todoText) {
		By todoLabelLocator = xpath(format(TODO_LABEL_BY_NAME_LOCATOR_PATTERN, todoText));
		WebElement todoLabel = driver.findElement(todoLabelLocator);
		new Actions(driver).moveToElement(todoLabel).perform();
		By todoRemoveButtonLocator = xpath(format(TODO_REMOVE_BUTTON_BY_NAME_LOCATOR_PATTERN, todoText));
		driver.findElement(todoRemoveButtonLocator).click();
	}

	public static void assertTodoIsPresent(String todoText) {
		By todoLocator = xpath(format(TODO_RECORD_BY_NAME_LOCATOR_PATTERN, todoText));
		assertTrue(
				format("Todo [%s] wasn't found in list", todoText),
				waitFor(1).successful(elementsToBecomeVisible(todoLocator))
		);
	}

	public static void assertTodoIsAbsent(String todoText) {
		By todoLocator = xpath(format(TODO_RECORD_BY_NAME_LOCATOR_PATTERN, todoText));
		assertTrue(
				format("Todo [%s] is still present in list", todoText),
				waitFor(1).successful(elementsToBecomeHiddenOrAbsent(todoLocator))
		);
	}

	public static void assertTodoItemsLeftCount(int expectedTodoCount) {
		int actualTodoCount = Integer.valueOf(driver.findElement(TODO_COUNT_TEXT_NODE).getText());
		assertTrue(
				format("Todo items left count expected to be %d, but was %d", expectedTodoCount, actualTodoCount),
				actualTodoCount == expectedTodoCount
		);
	}

	public static void assertFiltersAreHidden() {
		assertTrue(
				"Filters are still displayed",
				!driver.findElement(FILTERS).isDisplayed()
		);
	}

	public static void markTodoAsCompleted(String todoText) {
		By todoLocator = xpath(format(TODO_MARK_AS_COMPLETED_CHECKBOX_BY_NAME_LOCATOR_PATTERN, todoText));
		driver.findElement(todoLocator).click();
	}

	public static void applyFilter(String filterName) {
		By filterLocator = xpath(format(FILTER_BY_NAME_LOCATOR_PATTERN, filterName));
		driver.findElement(filterLocator).click();
	}

	public static void clearCompletedTodos() {
		driver.findElement(CLEAR_COMPLETED_BUTTON).click();
	}
}
