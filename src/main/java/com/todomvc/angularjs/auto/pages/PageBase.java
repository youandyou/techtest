package com.todomvc.angularjs.auto.pages;

import com.todomvc.angularjs.auto.waitings.CustomWait;

import static com.todomvc.angularjs.auto.DriverWrapper.driver;

public class PageBase {

	public static CustomWait waitFor(int timeout) {
		return new CustomWait(driver, timeout);
	}

	public static CustomWait waitFor(int timeout, int polling) {
		return new CustomWait(driver, timeout, polling);
	}
}
