package com.todomvc.angularjs.auto.steps;

import com.todomvc.angularjs.auto.pages.MainPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import org.slf4j.Logger;

import static com.todomvc.angularjs.auto.DriverWrapper.driver;
import static com.todomvc.angularjs.auto.pages.MainPage.assertMainPageIsLoaded;

public class StepDefs implements En {

	public static Logger logger;

	public static final int TIMEOUT_PAGE_LOAD = Integer.parseInt(System.getProperty("test.timeout.pageLoad", "30"));

	@Given("I am on the page (.*)")
	public static void openPage(String url) {
		driver.get(url);
		assertMainPageIsLoaded();
	}

	@When("I add todo \\[(.+)]")
	public static void addTodo(String todoText) {
		MainPage.addTodo(todoText);
	}

	@When("I remove todo \\[(.+)]")
	public static void removeTodo(String todoText) {
		MainPage.removeTodo(todoText);
	}

	@When("I mark todo as 'Completed': \\[(.+)]")
	public static void markTodoAsCompleted(String todoText) {
		MainPage.markTodoAsCompleted(todoText);
	}

	@When("I set filter to show '([a-zA-Z]+)' todos")
	public static void applyCompletedFilter(String filterName) {
		MainPage.applyFilter(filterName);
	}

	@When("I clear completed todos")
	public static void clearCompletedTodos() {
		MainPage.clearCompletedTodos();
	}

	@Then("I confirm todo is present in list: \\[(.+)]")
	public static void assertTodoIsPresent(String todoText) {
		MainPage.assertTodoIsPresent(todoText);
	}

	@Then("I confirm todo is absent in list: \\[(.+)]")
	public static void assertTodoIsAbsent(String todoText) {
		MainPage.assertTodoIsAbsent(todoText);
	}

	@Then("I confirm todo items left count is ([0-9]+)")
	public static void assertTodoItemsLeftCount(int todoCount) {
		MainPage.assertTodoItemsLeftCount(todoCount);
	}

	@Then("I confirm filters are hidden")
	public static void assertFiltersAreHidden() {
		MainPage.assertFiltersAreHidden();
	}
}
