package com.todomvc.angularjs.auto;

import com.google.common.base.MoreObjects;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import static com.google.common.base.Strings.emptyToNull;
import static com.todomvc.angularjs.auto.vocabulary.Symbols.COMMA;
import static com.todomvc.angularjs.auto.vocabulary.Symbols.SIZE_DELIMITER;
import static java.lang.String.format;

public class DriverWrapper {

	public static WebDriver driver;
	public static boolean driverOn = false;

	public static final String projectDir = System.getProperty("user.dir");
	public static final String srcPath = projectDir.concat("/src");
	public static final String resourcesPath = srcPath.concat("/main/resources");
	public static final String targetPath = projectDir.concat("/target");
	public static final String downloadDefaultDirectory = targetPath.concat("/downloads");

	private static final String CHROME_SWITCH_WINDOW_SIZE = "--window-size=";
	private static final String CHROME_WINDOW_POSITION = "--window-position=";

	private static final boolean startMaximized = Boolean.parseBoolean(System.getProperty("test.browser.start.maximized", "true"));

	public static void initDriver(String pageLoadStrategy) {
		if (driver == null) {
			driver = getDriver(pageLoadStrategy);
			driverOn = true;
			if (!(driver instanceof ChromeDriver)) {
				if (startMaximized) {
					driver.manage().window().maximize();
				} else {
					driver.manage().window().setPosition(getWindowPosition());
					driver.manage().window().setSize(getWindowSize());
				}
			}
		}
	}

	public static void quitDriver() {
		if (driverOn) {
			driver.quit();
			driverOn = false;
			driver = null;
		}
	}

	private static WebDriver getDriver(String pageLoadStrategy) {
		String browser = System.getProperty("test.browser", "Chrome");
		switch (browser) {
			case "Chrome":
				return getChromeDriver(pageLoadStrategy);
			case "Firefox":
				return getFirefoxDriver();
			default:
				throw new RuntimeException(format("There is no implementation to initialize driver for the following browser: %s", browser));
		}
	}

	private static ChromeDriver getChromeDriver(String pageLoadStrategy) {
		System.setProperty("webdriver.chrome.driver", resourcesPath.concat("/drivers/chromedriver.exe"));
		ChromeOptions options = new ChromeOptions();
		Map<String, Object> prefs = new HashMap<>();
		prefs.put("download.default_directory", downloadDefaultDirectory);
		prefs.put("credentials_enable_service", false);
		prefs.put("password_manager_enabled", false);
		prefs.put("pageLoadStrategy", pageLoadStrategy);
		options.setExperimentalOption("prefs", prefs);
		if (startMaximized) {
			options.addArguments("--start-maximized");
		} else {
			options.addArguments(CHROME_SWITCH_WINDOW_SIZE + getWindowSizeAsString());
			options.addArguments(CHROME_WINDOW_POSITION + getWindowPositionAsString());
		}
		LoggingPreferences loggingPreferences = new LoggingPreferences();
		loggingPreferences.enable(LogType.BROWSER, Level.ALL);
		loggingPreferences.enable(LogType.PERFORMANCE, Level.INFO);
		DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
		desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences);
		return new ChromeDriver(desiredCapabilities);
	}

	private static FirefoxDriver getFirefoxDriver() {
		System.setProperty("webdriver.gecko.driver", resourcesPath.concat("/drivers/geckodriver.exe"));
		FirefoxProfile ffProfile = new FirefoxProfile();
		ffProfile.setPreference("browser.download.dir", downloadDefaultDirectory);
		FirefoxOptions opts = new FirefoxOptions().setLogLevel(Level.OFF);
		DesiredCapabilities caps = opts.addTo(DesiredCapabilities.firefox());
		caps.setCapability(FirefoxDriver.PROFILE, ffProfile);
		return new FirefoxDriver(caps);
	}

	private static Dimension getWindowSize() {
		String[] coords = getWindowSizeAsString().split(COMMA);
		return new Dimension(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]));
	}

	private static String getWindowSizeAsString() {
		return getDefaultWindowSizeFormat().replace(SIZE_DELIMITER, COMMA);
	}

	private static Point getWindowPosition() {
		String[] coords = getWindowPositionAsString().split(COMMA);
		return new Point(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]));
	}

	private static String getWindowPositionAsString() {
		return System.getProperty("test.browser.window.position", "0,0");
	}

	private static String getDefaultWindowSizeFormat() {
		return MoreObjects.firstNonNull(emptyToNull(System.getProperty("test.browser.window.size")),
				"800" + SIZE_DELIMITER + "600");
	}
}
