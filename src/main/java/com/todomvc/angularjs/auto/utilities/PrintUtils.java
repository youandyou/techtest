package com.todomvc.angularjs.auto.utilities;

import java.util.Formatter;

public class PrintUtils {

	public static void printfn(String format, Object... args) {
		System.out.println(new Formatter().format(format, args).toString());
	}
}
