package com.todomvc.angularjs.auto.utilities;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.todomvc.angularjs.auto.vocabulary.Symbols.SPACE;
import static java.lang.String.format;

public class StringUtils {

	public static void checkIfMatches(Matcher matcher, String string) {
		if (!matcher.find()) {
			throw new RuntimeException(format("No matches:\r\nPattern: [%s]\r\nString: [%s]", matcher.pattern(), string));
		}
	}

	public static boolean stringContainsWords(String string, String... words) {
		return Arrays.stream(words)
				.map(String::toLowerCase)
				.allMatch(string.toLowerCase()::contains);
	}

	public static String prettifyFeature(String feature) {
		return Arrays
				.stream(feature.split("-"))
				.map(org.apache.commons.lang3.StringUtils::capitalize)
				.collect(Collectors.joining(SPACE));
	}

	public static String[] getGroups(String pattern, String string) {
		Matcher matcher = Pattern.compile(pattern).matcher(string);
		checkIfMatches(matcher, string);
		int groupCount = matcher.groupCount();
		String[] groups = new String[groupCount];
		for (int i = 1; i <= matcher.groupCount(); i++) {
			groups[i - 1] = matcher.group(i);
		}
		return groups;
	}
}
