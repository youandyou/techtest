package com.todomvc.angularjs.auto.vocabulary;

public class Symbols {

	public static final String UNDERSCORE = "_";
	public static final String PIPE = "|";
	public static final String SPACE = " ";
	public static final String COLON = ":";
	public static final String SEMICOLON = ";";
	public static final String DOT = ".";
	public static final String COMMA = ",";
	public static final String HASH = "#";
	public static final String EQUALS = "=";
	public static final String SIZE_DELIMITER = "x";
}
