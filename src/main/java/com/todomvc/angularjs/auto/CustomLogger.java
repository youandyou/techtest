package com.todomvc.angularjs.auto;

import org.apache.log4j.*;
import org.slf4j.impl.Log4jLoggerFactory;

import static com.todomvc.angularjs.auto.utilities.PrintUtils.printfn;

public class CustomLogger {

	private static final Logger LOGGER = Logger.getLogger("LOGGER");

	private static final String DL_CONVERSION_PATTERN = "[%-5p] %d{[HH:mm:ss.SSS]} %m%n";
	private static final String CL_CONVERSION_PATTERN = "> %m%n";

	private static final String RELATIVE_DL_PATH = "target\\logs\\debug.log";
	public static final String ABSOLUTE_DL_PATH = System.getProperty("user.dir").concat("\\").concat(RELATIVE_DL_PATH);

	public static org.slf4j.Logger getLogger() {
		if (!LOGGER.getAllAppenders().hasMoreElements()) {
			initLogger();
		}
		printfn("Writing DEBUG messages to %s", ABSOLUTE_DL_PATH);
		return new Log4jLoggerFactory().getLogger("LOGGER");
	}

	private static void initLogger() {
		System.out.println("Initializing logger..");

		PatternLayout patternLayout_console = new PatternLayout();
		patternLayout_console.setConversionPattern(CL_CONVERSION_PATTERN);

		ConsoleAppender fileAppender = new ConsoleAppender();
		fileAppender.setName("CA");
		fileAppender.setTarget("System.out");
		fileAppender.setThreshold(Level.INFO);
		fileAppender.setLayout(patternLayout_console);
		fileAppender.activateOptions();

		LOGGER.addAppender(fileAppender);
		LOGGER.setLevel(Level.INFO);

		PatternLayout patternLayout_debug = new PatternLayout();
		patternLayout_debug.setConversionPattern(DL_CONVERSION_PATTERN);

		RollingFileAppender debuggerFileAppender = new RollingFileAppender();
		debuggerFileAppender.setName("DFA");
		debuggerFileAppender.setThreshold(Level.DEBUG);
		debuggerFileAppender.setLayout(patternLayout_debug);
		debuggerFileAppender.setFile(RELATIVE_DL_PATH);
		debuggerFileAppender.activateOptions();

		LOGGER.addAppender(debuggerFileAppender);
		LOGGER.setLevel(Level.DEBUG);
	}
}
