@regression
Feature: Regression Suite

	Scenario: Add several todos, change statuses to some of them, verify filters and todos counter
		Given I am on the page http://todomvc.com/examples/angularjs/#/active

		When I add todo [1. Set up registration desk]
		Then I confirm todo is present in list: [1. Set up registration desk]
		And I confirm todo items left count is 1

		When I add todo [2. Get badges ready]
		Then I confirm todo is present in list: [2. Get badges ready]
		And I confirm todo items left count is 2

		When I add todo [3. Print agendas]
		Then I confirm todo is present in list: [3. Print agendas]
		And I confirm todo items left count is 3

		When I add todo [4. Seating arrangements done]
		Then I confirm todo is present in list: [4. Seating arrangements done]
		And I confirm todo items left count is 4

		When I mark todo as 'Completed': [2. Get badges ready]
		Then I confirm todo items left count is 3

		When I mark todo as 'Completed': [3. Print agendas]
		Then I confirm todo items left count is 2

		And I set filter to show 'Completed' todos
		Then I confirm todo is absent in list: [1. Set up registration desk]
		And I confirm todo is present in list: [2. Get badges ready]
		And I confirm todo is present in list: [3. Print agendas]
		And I confirm todo is absent in list: [4. Seating arrangements done]
		And I confirm todo items left count is 2

		When I set filter to show 'Active' todos
		Then I confirm todo is present in list: [1. Set up registration desk]
		And I confirm todo is absent in list: [2. Get badges ready]
		And I confirm todo is absent in list: [3. Print agendas]
		And I confirm todo is present in list: [4. Seating arrangements done]
		And I confirm todo items left count is 2

		When I set filter to show 'All' todos
		Then I confirm todo is present in list: [1. Set up registration desk]
		And I confirm todo is present in list: [2. Get badges ready]
		And I confirm todo is present in list: [3. Print agendas]
		And I confirm todo is present in list: [4. Seating arrangements done]
		And I confirm todo items left count is 2

		When I clear completed todos
		Then I confirm todo is present in list: [1. Set up registration desk]
		And I confirm todo is absent in list: [2. Get badges ready]
		And I confirm todo is absent in list: [3. Print agendas]
		And I confirm todo is present in list: [4. Seating arrangements done]
		And I confirm todo items left count is 2

		When I remove todo [4. Seating arrangements done]
		Then I confirm todo is absent in list: [4. Seating arrangements done]
		And I confirm todo items left count is 1

		When I remove todo [1. Set up registration desk]
		Then I confirm todo is absent in list: [1. Set up registration desk]
		And I confirm filters are hidden