@smoke
Feature: Smoke Suite

	Scenario: Add todo, mark it as 'Completed', verify filters and remove todo
		Given I am on the page http://todomvc.com/examples/angularjs/#/active

		When I add todo [1. Set up registration desk]
		Then I confirm todo is present in list: [1. Set up registration desk]

		When I mark todo as 'Completed': [1. Set up registration desk]
		And I set filter to show 'Completed' todos
		Then I confirm todo is present in list: [1. Set up registration desk]

		When I set filter to show 'Active' todos
		Then I confirm todo is absent in list: [1. Set up registration desk]

		When I set filter to show 'All' todos
		Then I confirm todo is present in list: [1. Set up registration desk]

		When I remove todo [1. Set up registration desk]
		Then I confirm todo is absent in list: [1. Set up registration desk]