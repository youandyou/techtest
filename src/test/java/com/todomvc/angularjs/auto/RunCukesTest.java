package com.todomvc.angularjs.auto;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import static com.todomvc.angularjs.auto.CustomLogger.getLogger;
import static com.todomvc.angularjs.auto.steps.StepDefs.logger;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumber/cucumber.json", overviewReport = true, outputFolder = "target/cucumber/overview")
@CucumberOptions(
		tags = {},
		plugin = {"html:target/cucumber/cucumber-html-report",
				"json:target/cucumber/cucumber.json",
				"usage:target/cucumber/cucumber-usage.json",
				"junit:target/cucumber/cucumber-results.xml"},
		features = {"src/features/"}
)
public class RunCukesTest {

	@BeforeClass
	public static void before() {
		logger = getLogger();
	}

	@AfterClass
	public static void after() {
	}
}
