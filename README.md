##To successfully run tests you should have:
* JDK 8 installed
* JAVA_HOME set up
* Apache Maven 3 installed
* M2_HOME set up
* Chrome 67-69 installed (in case you want to run tests in Chrome)
* Firefox 57 (and greater) installed (in case you want to run tests in Firefox)

##Batch-files to run tests can be found in the project's root directory.