mvn clean test ^
-Dsurefire.useSystemClassLoader=false ^
-DforkCount=0 ^
-Dtest.browser=Chrome ^
-Dtest.browser.start.maximized=true ^
-Dcucumber.options="--tags @smoke --tags ~@ignore" ^
-Dtest.timeout.pageLoad=20