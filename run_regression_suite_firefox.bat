mvn clean test ^
-Dsurefire.useSystemClassLoader=false ^
-DforkCount=0 ^
-Dtest.browser=Firefox ^
-Dtest.browser.start.maximized=true ^
-Dcucumber.options="--tags @regression --tags ~@ignore" ^
-Dtest.timeout.pageLoad=20